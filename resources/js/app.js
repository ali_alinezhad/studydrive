require('./bootstrap');

window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('gallery', require('./components/Gallery.vue').default);
Vue.component('photo-modal', require('./components/PhotoModal.vue').default);
Vue.component('pagination', require('./components/Pagination.vue').default);
Vue.component('active-user', require('./components/guest/ActiveUsers.vue').default);


import Pagination from "./components/Pagination";
const app = new Vue({
    el: '#app',
    components: {
        pagination: Pagination,
    },
    data () {
        return {
            currentPage: 1,
        };
    },
    methods: {
        onPageChange(page) {
            this.currentPage = page;
            window.location.href = "/show/photos/?start="+(page * 100 +1);
        }
    },
});
