@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div>
                        <ul class="navbar list-inline">
                            <li>
                                <a href="{{route('picked.photos',['favorite'=> 1])}}">Your Favorite List</a>
                            </li>
                            <li>
                                <a href="{{route('picked.photos',['favorite'=> 0])}}"> Your UnFavorite List</a>
                            </li>
                        </ul>
                    </div>
                    <gallery
                        :photos="<?php echo htmlspecialchars(json_encode($photos), ENT_QUOTES, 'UTF-8', true); ?>"
                        :check=true>
                    </gallery>
                </div>
            </div>
        </div>
    </div>
@endsection
