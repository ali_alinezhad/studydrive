@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                        <ul class="navbar list-inline">
                        <li>
                            <a href="{{route('picked.photos',['favorite'=> 1])}}">Your Favorite List</a>
                        </li>
                        <li>
                            <a href="{{route('picked.photos',['favorite'=> 0])}}"> Your UnFavorite List</a>
                        </li>
                    </ul>
                </div>

                <gallery
                    :photos="<?php echo htmlspecialchars(json_encode($photos), ENT_QUOTES, 'UTF-8', true); ?>"
                    :picked="<?php echo htmlspecialchars(json_encode($pickedPhoto), ENT_QUOTES, 'UTF-8', true); ?>"
                    :show-all=true
                    ></gallery>

                <pagination
                    :total-pages="49"
                    :total="5000"
                    :per-page="100"
                    :current-page="currentPage"
                    @pagechanged="onPageChange"></pagination>

                <script type="text/x-template" id="pagination">
                    <ul class="pagination">
                        <li class="pagination-item">
                            <button
                                type="button"
                                @click="onClickFirstPage"
                                :disabled="isInFirstPage"
                                aria-label="Go to first page">First
                            </button>
                        </li>

                        <li class="pagination-item">
                            <button
                                type="button"
                                @click="onClickPreviousPage"
                                :disabled="isInFirstPage"
                                aria-label="Go to previous page">Previous
                            </button>
                        </li>

                        <li v-for="page in pages" class="pagination-item">
                            <button
                                type="button"
                                @click="onClickPage(page.name)"
                                :disabled="page.isDisabled"
                                :class="{ active: isPageActive(page.name) }"
                                :aria-label="`Go to page number ${page.name}`">
                            </button>
                        </li>

                        <li class="pagination-item">
                            <button
                                type="button"
                                @click="onClickNextPage"
                                :disabled="isInLastPage"
                                aria-label="Go to next page">Next
                            </button>
                        </li>

                        <li class="pagination-item">
                            <button
                                type="button"
                                @click="onClickLastPage"
                                :disabled="isInLastPage"
                                aria-label="Go to last page">Last
                            </button>
                        </li>
                    </ul>
                </script>
            </div>
        </div>
    </div>
</div>
@endsection
