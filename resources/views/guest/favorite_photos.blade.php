@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <active-user :users="{{ $users }}" :is-weekend ="{{ $isWeekend }}"></active-user>

                    @if(!$isWeekend)
                        <div class="container">
                            @foreach ($photos as $photo)
                               <img class="pb-1" src="{{ $photo->thumbnailUrl }}">
                            @endforeach
                        </div>
                    @endif

                    <div class="p-3">
                        {{ $photos->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
