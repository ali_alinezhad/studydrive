<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Photo;
use Faker\Generator as Faker;

$factory->define(Photo::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'photo_id' => $faker->biasedNumberBetween(),
        'title'    => $faker->title,
        'url'      => $faker->url,
        'thumbnailUrl' => $faker->url,
        'is_favorite' => 1,
        'created_at' => now(),
    ];
});
