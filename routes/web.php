<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Guests Panel
Route::get('/favorite/photos', 'GuestController@index')->name('guest.home');

// Registered User Panel
Route::get('/home', 'UserController@index')->name('home');
Route::get('/show/photos', 'UserController@showAllPhotos')->name('show.photos');
Route::get('/add/to/PickedList', 'UserController@addToPickedList')->name('add.to.picked.list');
Route::delete('/remove/from/PickedList', 'UserController@removeFromPickedList')->name('remove.from.picked.list');
Route::get('/show/list/{favorite}/photos', 'UserController@showPickedPhotos')->name('picked.photos');

Auth::routes();
