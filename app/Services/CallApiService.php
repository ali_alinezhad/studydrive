<?php

declare(strict_types=1);

namespace App\Services;


use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CallApiService
{
    /**
     * @param $request
     * @return mixed
     */
    public function getPhotoFromAPI($request)
    {
       // $response = Cache::remember('photo', 36000, function () use($request) {
            $client = new Client();
            $res = $client->get(config('api.image_api') . '/photos', [
                'query' => [
                    '_start' => $request->get('start'),
                    '_limit' => 20
                    ]
            ]);

            if (Response::HTTP_OK === $res->getStatusCode()) {
                $response = $res->getBody()->getContents();
            }

          //  return [];
       // });

        return json_decode($response, true);
    }
}
