<?php

namespace App\Http\Controllers;


use App\Repositories\UserRepository;
use App\Services\CallApiService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @param UserRepository $repository
     * @param CallApiService $apiService
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAllPhotos(UserRepository $repository, CallApiService $apiService, Request $request)
    {
        return view('photo.show_all',[
            'photos' => $apiService->getPhotoFromAPI($request),
            'pickedPhoto' => $repository->getAllPickedPhotos()
        ]);
    }

    /**
     * @param UserRepository $repository
     * @param $isFavorite
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPickedPhotos(UserRepository $repository, $isFavorite)
    {
        return view('photo.show_picked',[
            'photos' => $repository->getPickedPhotos($isFavorite)
        ]);
    }

    /**
     * @param Request $request
     * @param UserRepository $repository
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToPickedList(Request $request, UserRepository $repository)
    {
        $repository->addPhotoToFavoriteOrUnFavoriteList($request);

        return response()->json(
            [
                'success' => true,
                'message' => 'Photo added successfully'
            ]
        );
    }

    public function removeFromPickedList(Request $request, UserRepository $repository)
    {
        $repository->removePhotoFromFavoriteOrUnFavoriteList($request);
        return response()->json(
            [
                'success' => true,
                'message' => 'Photo added successfully'
            ]
        );
    }
}
