<?php

namespace App\Http\Controllers;


use App\Repositories\GuestRepository;

class GuestController extends Controller
{
    /**
     * @param GuestRepository $repository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GuestRepository $repository)
    {
        return view('guest.favorite_photos',[
            'users'     => $repository->getActiveUsers(),
            'photos'    => $repository->getFavoritePhotos(),
            'isWeekend' => $repository->checkIsWeekEnd()
        ]);
    }
}
