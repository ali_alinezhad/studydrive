<?php

namespace App\Repositories\Interfaces;



interface GuestInterface
{
    /**
     * @return object
     */
    public function getActiveUsers() : object;

    /**
     * @return object
     */
    public function getFavoritePhotos() : object;

    /**
     * @return int
     */
    public function checkIsWeekEnd() : int;
}
