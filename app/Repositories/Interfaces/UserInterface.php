<?php


namespace App\Repositories\Interfaces;


interface UserInterface
{
    /**
     * @return int
     */
    public function getCurrentUserId() : int;

    /**
     * @return array
     */
    public function getAllPickedPhotos() : array;

    /**
     * @param $isFavorite
     * @return object
     */
    public function getPickedPhotos($isFavorite) : object;

    /**
     * @param $request
     */
    public function addPhotoToFavoriteOrUnFavoriteList($request) : void;

    /**
     * @param $request
     */
    public function removePhotoFromFavoriteOrUnFavoriteList($request) : void;
}
