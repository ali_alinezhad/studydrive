<?php


namespace App\Repositories;


use App\Photo;
use App\Repositories\Interfaces\UserInterface;
use Illuminate\Support\Facades\Auth;


class UserRepository implements UserInterface
{
    /**
     * @return int
     */
    public function getCurrentUserId() : int
    {
        return Auth::user()->getAuthIdentifier();
    }

    /**
     * @return array
     */
    public function getAllPickedPhotos(): array
    {
        $photos = Photo::whereUserId($this->getCurrentUserId())->get();
        $photoId = [];
        foreach($photos as $photo) {
            $photoId[] = $photo->photo_id;
        }

        return $photoId;
    }

    /**
     * @param $isFavorite
     * @return object
     */
    public function getPickedPhotos($isFavorite): object
    {
        return Photo::whereUserIdAndIsFavorite($this->getCurrentUserId(), $isFavorite)->get();
    }

    /**
     * @param $request
     */
    public function addPhotoToFavoriteOrUnFavoriteList($request): void
    {
        $photo = new Photo();

        $photo->user_id       = $this->getCurrentUserId();
        $photo->photo_id      = $request->get('id');
        $photo->title         = $request->get('title');
        $photo->url           = $request->get('url');
        $photo->thumbnailUrl  = $request->get('thumbnailUrl');
        $photo->is_favorite   = $request->get('isFavorite');;

        $photo->save();
    }

    /**
     * @param $request
     */
    public function removePhotoFromFavoriteOrUnFavoriteList($request): void
    {
        Photo::find($request->get('id'))->delete();
    }
}
