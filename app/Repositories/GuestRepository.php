<?php


namespace App\Repositories;


use App\Repositories\Interfaces\GuestInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class GuestRepository implements GuestInterface
{
    /**
     * @return object
     */
    public function getActiveUsers() : object
    {
        $date = Carbon::today()->subDays(7);

        return DB::table('users')
            ->join('photos', 'users.id', '=', 'photos.user_id')
            ->where('photos.is_favorite','=', 1)
            ->where('photos.created_at','>=',$date)
            ->select('users.name','users.email','photos.user_id')
            ->groupBy('photos.user_id')
            ->get();
    }

    /**
     * @return object
     */
    public function getFavoritePhotos() : object
    {
        return DB::table('photos')->where('is_favorite',1)->Paginate(20);
    }

    /**
     * @return int
     */
    public function checkIsWeekEnd() : int
    {
        return Carbon::now()->isWeekend() ? 1 : 0;
    }
}
