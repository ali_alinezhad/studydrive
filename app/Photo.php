<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Media.
 *
 * @property int            $id
 * @property int            $photo_id
 * @property int            $user_id
 * @property string         $title
 * @property string         $url
 * @property string         $thumbnailUrl
 * @property boolean        $is_favorite
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Photo extends Model
{
    protected $fillable = [
        'photo_id',
        'user_id',
        'title',
        'url',
        'thumbnailUrl',
        'is_favorite'
    ];

    protected $table = 'photos';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
