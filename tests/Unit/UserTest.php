<?php


namespace Tests\Unit;


use App\Photo;
use App\Repositories\UserRepository;
use App\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testUserExist()
    {
        $user = factory(User::class)->create();
        $this->assertDatabaseHas('users', [
            'email' => $user->email,
        ]);
    }

    public function testGetAllPickedPhotos()
    {
        $stub = $this->createMock(UserRepository::class);
        $stub->method('getAllPickedPhotos')->willReturn([1,2,3]);
        $this->assertEquals([1,2,3],$stub->getAllPickedPhotos());
    }

    public function testGetPickedPhotos()
    {
        $photo = factory(Photo::class)->create();
        $stub  = $this->createMock(UserRepository::class);
        $stub->method('getPickedPhotos')->willReturn($photo);
        $this->assertEquals($photo,$stub->getPickedPhotos(1));
    }

    public function testAddPhotoToFavoriteOrUnFavoriteList()
    {
        $photo = factory(Photo::class)->create();
        $stub  = $this->createMock(UserRepository::class);
        $stub->method('addPhotoToFavoriteOrUnFavoriteList');
        $stub->addPhotoToFavoriteOrUnFavoriteList($photo);
        $this->assertDatabaseHas('photos', [
            'photo_id' => $photo->photo_id,
        ]);
    }
}
