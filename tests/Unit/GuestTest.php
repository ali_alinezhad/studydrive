<?php


namespace Tests\Unit;


use App\Photo;
use App\Repositories\GuestRepository;
use App\User;
use Tests\TestCase;

class GuestTest extends TestCase
{
    public function testGetActiveUsers()
    {
        $user = factory(User::class)->create();
        $stub  = $this->createMock(GuestRepository::class);
        $stub->method('getActiveUsers')->willReturn($user);
        $this->assertEquals($user,$stub->getActiveUsers());
    }
    public function testGetFavoritePhotos()
    {
        $photo = factory(Photo::class)->create();
        $stub  = $this->createMock(GuestRepository::class);
        $stub->method('getFavoritePhotos')->willReturn($photo);
        $this->assertEquals($photo,$stub->getFavoritePhotos());
    }
}
