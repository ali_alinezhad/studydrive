<?php


namespace Tests\Feature;


use App\User;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    public function testShowAllPhotos()
    {
        $user = factory(User::class)->create();
        $response = $this->post(route('login'), [
            'email' => $user->email,
            'password' => 'password'
        ]);
        $response = $this->get('/show/photos');
        $response->assertStatus(200);
    }
}
