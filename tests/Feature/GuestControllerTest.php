<?php


namespace Tests\Feature;


use App\User;
use Tests\TestCase;

class GuestControllerTest extends TestCase
{
   public function testRegister()
    {
        $response = $this->get('/register');
        $response->assertStatus(200);
        $this->withoutExceptionHandling();
        $user = factory(User::class)->make()->toArray();
        $this->assertNotEquals(0, User::count());
    }

    public function testLogin()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
        $user = factory(User::class)->create();
        $response = $this->post(route('login'), [
            'email' => $user->email,
            'password' => 'password'
        ]);
        $response->assertRedirect(route('home'));
        $this->assertAuthenticatedAs($user);
    }

    public function testIndex()
    {
        $response = $this->get('/favorite/photos');
        $response->assertStatus(200);
    }
}
